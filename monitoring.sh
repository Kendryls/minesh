#!/bin/sh
. $(dirname $(readlink -f "$0"))/serv-config.cfg #Inportation des config general
. $(dirname $(readlink -f "$0"))/Mineicha/function.sh #Inportation des fonctions et variable global
#--------------------------------------------------------------------------
#Recuperation du PID du serveur
#--------------------------------------------------------------------------

pid=$(jps -l | grep vanilla | cut -d' ' -f1) #Recupere la ligne du jps qui contient le proc du serveur (grep) et extrait juste son PID (cut)

#echo "$pid"
if [ $1 = 'memoire' ]
then
 	  mem=$(ps -p $pid -o %mem=)
	  echo "Memoire utiliser a $mem%"

elif [ $1 = 'cpu' ]
then
	  cpu=$(ps -p $pid -o %cpu=)
	  echo "Charge CPU a $cpu%"
elif [ $1 = 'help' ]
then

	echo " "
	echo " "
	echo "--------------------------- HELP "
	echo "${ORANGE}memoire:${NC} affiche la consomation de la ram"
	echo "${ORANGE}cpu:${NC} affiche la consomation du cpu"
	echo "--------------------------- By Ichamaru"
	echo " "
	echo " "

else
        echo "${ORANGE}Veuillez entrer un parametre valide. utiliser le parametre help pour plus d'informations${NC}"
fi
