#!/bin/sh

. config/serv-config.cfg
. function/color.sh
. function/main.sh

#--------------------------------------------------------------------------
#Executions en fonctions du ou des parametres
#--------------------------------------------------------------------------
verif_user
if [ $1 = 'start' ]
then
		check_screen
	    if [ $screen = "true" ]
        then
            echo "${RED}Le serveur est deja en cours d'execution${NC}"
        else
			demmarer # J'appelle la fonction demmarer
        fi

elif [ $1 = 'stop' ]
then
		check_screen
	    if [ $screen = "true" ]
        then
			arreter  # J'appelle la fonction arreter
		else
			echo "${RED}Le serveur n'est pas en cours d'execution${NC}"
		fi

elif [ $1 = 'hard-stop' ]
then
		check_screen
	    arret_forcer  # J'appelle la fonction arreter_forcer

elif [ $1 = 'restart' ]
then
		check_screen
	    if [ $screen = "true" ]
        then
			echo "${ORANGE}Redemarage du serveur en cours${NC}"
			arreter  # J'appelle la fonction arreter
        sleep 3
			demmarer # J'appelle la fonction demmarer
		else
			echo "${RED}Le serveur n'est pas en cours d'execution${NC}"
		fi

elif [ $1 = 'backup' ]
then
		check_screen
        echo "${ORANGE}Creationt d'une backup de la map${NC}"

		if [ $screen = "true" ]
			then
				say "Backup du monde en cours de creation"
		fi
        cd ..
		zip -r $locate_backup/$Jour-$Heure.zip world_* plugins server.properties crash-reports
		find $locate_backup -type f -name '*.zip' -atime +$max_backup -exec rm -rf {} \; #suppresion des backups en surplus (definit dans $max_backup)

		if [ $screen = "true" ]
			then
				say "Backup de la map terminée"
		fi


elif [ $1 = 'save' ]
then
		check_screen
		if [ $screen = "true" ]
        then
			echo "${ORANGE}Sauvegarde de la map [Inventaire,chunck ect]${NC}"
			#say "Sauvegarde des mondes"
			com_exec "screen -S $screen_name -X -p0 eval \"stuff 'save-all'^m\""

		else
			echo "${RED}Le serveur n'est pas en cours d'execution${NC}"
		fi



elif [ $1 = 'say' ] #envoyer un message dans le chat du serveur ;)
then
	 check_screen
     echo "Saisisez le message a envoyer:"
     read message

	 if [ -z "$message" ]
	   then
			echo "${RED}Le message est vide. ${NC}"
       else
			if [ $screen = "true" ]
			then
				say "$message"
				echo "${GREN}Message envoyer${NC}"
			else
				echo "${RED}Le serveur n'est pas en cours d'execution${NC}"
		fi

       fi


elif [ $1 = 'help' ]
then
echo " "
echo " "
echo "--------------------------- HELP "
echo "${ORANGE}start:${NC} Demmarage du serveur"
echo "${ORANGE}stop:${NC} Arret du serveur proprement"
echo "${ORANGE}hard-stop:${NC} Methode sauvage, kill le screen directement. ${RED}A utiliser en dernier recours${NC}"
echo "${ORANGE}restart:${NC} Redemarre le serveur"
echo "${ORANGE}backup:${NC} cree une backup dans le dossier backup en format ZIP et supprime les backups vielles de plus de $max_backup jours"
echo "${ORANGE}save:${NC} Lance la commande de save (save-all) du monde (chunck, inventaire ect) dans la console du serveur minecraft"
echo "${ORANGE}say:${NC} Envoi un message dans le chat du serveur minecraft"
echo "--------------------------- By Ichamaru"
echo " "
echo " "

#elif [ $1 = 'test' ]
#then



else
        echo "${ORANGE}Veuillez entrer un parametre valide. utiliser le parametre help pour plus d'informations${NC}"
fi
